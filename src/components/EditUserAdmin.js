import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function EditUserAdmin({user, fetchData}){

	const [userId, setUserId] = useState('')
	const [email, setEmail] = useState('');
	const [showEdit, setShowEdit] = useState(false);
	const [isAdmin, setIsAdmin] = useState(false);

	const navigate = useNavigate();

	const openEdit = (userId) => {
		// to get the actual data from the form
		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUserId(data._id);
			setEmail(data.email);
			setIsAdmin(data.isAdmin); 
			
		})
		//Then open the modal.
		setShowEdit(true);
	}

	const closeEdit = () => {
		setShowEdit(false);
		setEmail('');
		navigate('/users-all');
	}

	const editUser = (e, userId) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				email:email,
				isAdmin: isAdmin
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true ){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'User Successfully Updated!'
				})
				closeEdit();
				//para maupdate or refresh ang data
				// galing kay courses.js nilagay sa function ang fetchdata
				fetchData();
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please try again'
				})
				closeEdit();
				//para maupdate or refresh ang data
				fetchData();
			}
		})
	}

	return (

		<>
		<Button variant="primary" size="sm" onClick={() => openEdit(user)}>Edit</Button>

		{/*Edit Modal*/}
		<Modal show={showEdit} onHide={closeEdit}>
			<Form onSubmit={e => editUser(e, userId)}>
				<Modal.Header closeButton>
		          	<Modal.Title>Edit User</Modal.Title>
		        </Modal.Header>

		        <Modal.Body>
		        	<Form.Group controlId="productName">
		        	  <Form.Label>Email</Form.Label>
		        	  <Form.Control 
		        	  	type="text" 		        	  	  
		        	  	required
		        	  	value={email}
		        	  	onChange={e => {setEmail(e.target.value)}}
		        	  />
		        	</Form.Group>
		        </Modal.Body>

		        <Form.Group controlId="isAdmin" className="px-3">
	              <Form.Label>Is Admin</Form.Label>
	              <Form.Check
	                type="radio"
	                label="Yes"
	                name="isAdmin"
	                value={true}
	                checked={isAdmin === true}
	                onChange={() => setIsAdmin(true)}
	              />
	              <Form.Check
	                type="radio"
	                label="No"
	                name="isAdmin"
	                value={false}
	                checked={isAdmin === false}
	                onChange={() => setIsAdmin(false)}
	              />
	            </Form.Group>

		        <Modal.Footer>
		        	<Button variant="secondary" onClick={closeEdit} >Close</Button>
		        	<Button variant="success" type="submit">Submit</Button>
		        </Modal.Footer>
			</Form>

		</Modal>
		</>

	)
}