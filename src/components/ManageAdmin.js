import { useState, useEffect } from 'react';
import {Table, Button} from 'react-bootstrap';

import EditUserAdmin from './EditUserAdmin';


export default function ChangeAdmin({usersData, fetchData}) {

	const [users, setUsers] = useState([]);

	useEffect(() => {
		const usersArray = usersData.map(user => {
			// console.log(user)
			return (
				<tr key={user._id}>
					<td>{user._id}</td>
	                <td>{user.email}</td>
	                <td className={user.isAdmin ? "text-success" : "text-danger"}>{user.isAdmin ? 'Yes' : 'No'}</td>
	                <td className="text-center"><EditUserAdmin user={user._id} fetchData={fetchData}/></td>	
				</tr>
			)
		})

		setUsers(usersArray)

	}, [usersData])

	return(

		<>
			<h1 className="text-center my-4"> Users</h1>
			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>ID</th>
						<th>Email</th>
						<th>Admin</th>
 						<th className="text-center" colSpan="2">Actions</th>
					</tr>
				</thead>

				<tbody>
					{users}
				</tbody>
			</Table>	
		</>


	)
}