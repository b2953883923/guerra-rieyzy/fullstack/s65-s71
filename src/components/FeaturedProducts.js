import React from 'react';


export default function FeaturedProducts(){

	const featuredProducts = [
    {
      id: 1,
      name: 'Air Jordan 2',
      image: '/images/airjordan2.jpg',
      price: '10,000',
    },
    {
      id: 2,
      name: 'Air Jordan 1',
      image: '/images/jordan1.jpg',
      price: '9,000',
    },
    {
      id: 3,
      name: 'Air Jordan 13',
      image: '/images/jordan13.jpg',
      price: '13,000',
    },
  ];
	return(
	
	<div className="container" id="featured">
      <div className="featured-products text-center bg-secondary">
        <h2 className="p-3">Featured Products</h2>
        <div className="row">
          {featuredProducts.map((product) => (
            <div className="col-md-4 col-sm-12" key={product.id}>
              <div className="product-item">
                <img src={product.image} alt={product.name} />
                <h3>{product.name}</h3>
                <p><span>&#8369;</span>{product.price}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>


	)
}
