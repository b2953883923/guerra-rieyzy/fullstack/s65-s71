import Swal from 'sweetalert2';
import { Button } from 'react-bootstrap'

export default function ArchiveProduct({product, isActive, fetchData}) {


	const ArchiveToggle = (product) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${product}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
				if(data === true ) {
					Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: "Product successfully disabled",
				})
					fetchData();
				} else {
					Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: "Please try again!",
				})
					fetchData();
				}
				
		})
		fetchData();
	}

	const ActivateToggle = (product) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${product}/activate`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			
				if(data === true ) {
					Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: "Course successfully activated",
				})
					fetchData();
				} else {
					Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: "Please try again!",
				})
					fetchData();
				}
		})
		fetchData();
	}

	return(
		<>
		{isActive ? 
        <Button variant='danger' onClick={() => ArchiveToggle(product)}>Archive</Button>
     	  : 
        <Button variant='success' onClick={() => ActivateToggle(product)}>Activate</Button>
     	}

	    </>


	)
}