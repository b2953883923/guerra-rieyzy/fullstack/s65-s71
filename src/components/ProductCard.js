
import { Row, Col, Card, Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {
	const {_id, name, description, price } = productProp;


	return (
        <Container className="mt-3">
		 <Row>
            <Col xs={12} md={6} lg={{ span: 6, offset: 3 }}>
                <Card className="cardHighlight p-3 text-center ">
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text><span>&#8369;</span>{price}</Card.Text>
                        {/* Directs to the page of ProductView */}
                        <Button as={Link} to={`/products/${_id}`} variant="primary">View Details</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
        </Container>
	)
}