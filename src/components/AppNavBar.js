import { useContext} from 'react';
import {Navbar, Nav, Container, Carousel, NavDropdown} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { FaCartShopping } from "react-icons/fa6";

export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return (
	<div id="landing-page">
		<Navbar expand="lg" className="bg-light text-white" id="transparent-nav">
		      <Container>
		        <Navbar.Brand as={Link} to="/" >AD Sneaky PH</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as={NavLink} to="/" >Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>

		  

		             
					{
		              	(user.id !== null) ?
		              	
		              		user.isAdmin 
          					?
          					<>	
          						<Nav.Link as={Link} to="/users-all" id="user">Users</Nav.Link>
          						<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
          					</>
          					:
          					<>
          						<Nav.Link as={Link} to="/orders-user" id="order">Orders</Nav.Link>
          						<Nav.Link as={Link} to="/cart" id="order"><FaCartShopping/></Nav.Link>

          						<NavDropdown id="basic-nav-dropdown">
          						
          						{/*<Nav.Link as={Link} to="/logout">Logout</Nav.Link>*/}
          						<NavDropdown.Item as={Link} to="/profile">Profile</NavDropdown.Item>
          						<NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
          						</NavDropdown>
          					</>
		              	:
			              	<>
				              	<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
				              	<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
				            </>
		            }
				            
		            
		          
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		 </div>
	)

};
