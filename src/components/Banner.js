import { Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";

	// data is inside the curly bracket because data is an object
export default function Banner({data}) {

	const {title, content, destination,label} = data;

	return (

		<Row id="banner-font">
			<Col className="p-5 text-center">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn" to={destination} id="home-button"> {label} </Link>
			</Col>
		</Row>


	)
};