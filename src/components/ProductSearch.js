import React, { useState } from 'react';
import ProductCard from './ProductCard';

export default function ProductSearch(){

	const [searchQuery, setSearchQuery] = useState('');
  	const [searchResults, setSearchResults] = useState([]);

  	const Search = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/search`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });

      const data = await response.json();
      console.log(data)
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for courses:', error);
    }
  };

	return(
	<div className="container mt-4">
    	<div className="row">

		 <div className="col-md-6 offset-md-3">
	      <h2>Product Search</h2>
	      <div className="form-group">
	        <label htmlFor="productName">Product Name:</label>
	        <input
	          type="text"
	          id="productName"
	          className="form-control"
	          value={searchQuery}
	          onChange={event => setSearchQuery(event.target.value)}
	        />
	      </div>
	      <button className="btn btn-primary my-4" onClick={Search}>
	        Search
	      </button>
	      	<div>
		      <ul>
		        {searchResults.map(product => (
		          <ProductCard productProp={product} key={product._id}/>
		        ))}
		      </ul>
	      	</div>
	    </div>
	    </div>
	</div>

	)
}