import React, { useState, useEffect } from 'react';

export default function ProductSearchByPrice(){

  const [minPrice, setMinPrice] = useState('');
    const [maxPrice, setMaxPrice] = useState('');
    const [products, setProducts] = useState([]);

    const Search = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          minPrice: parseFloat(minPrice),
          maxPrice: parseFloat(maxPrice)
        }),
      });

      if (!response.ok) {
        throw new Error('An error occurred while searching for products');
      }

      const data = await response.json();
      console.log(data)
      setProducts(data);
    } catch (error) {
      console.error(error);
    }
  };

  

  return(
  <div className="container mt-4">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <h3>Product Search by Price</h3>
          <div className="mb-3">
            <label htmlFor="minPrice" className="form-label">
              Minimum Price
            </label>
            <input
              type="number"
              className="form-control"
              id="minPrice"
              value={minPrice}
              onChange={(e) => setMinPrice(e.target.value)}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="maxPrice" className="form-label">
              Maximum Price
            </label>
            <input
              type="number"
              className="form-control"
              id="maxPrice"
              value={maxPrice}
              onChange={(e) => setMaxPrice(e.target.value)}
            />
          </div>
          <button className="btn btn-primary" onClick={Search}>
            Search
          </button>
          {products?.length > 0 && (
            <div className="mt-4">
              <h4>Search Results:</h4>
              <ul>
                {products.map(product => (
                  <li key={product._id}>{product.name} - ${product.price}</li>
                ))}
              </ul>
            </div>
          )}
        </div>
      </div>
    </div>

  )
}