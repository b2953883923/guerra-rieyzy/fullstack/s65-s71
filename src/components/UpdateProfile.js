import React, { useState } from 'react';
import {Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

const UpdateProfileForm = ({fetchDetails}) => {
  const [email, setEmail] = useState('');
  

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    // Update the profile API endpoint
    const apiUrl = `${process.env.REACT_APP_API_URL}/users/profile`;
    const token = localStorage.getItem('token');

    try {
      const response = await fetch(apiUrl, {
        method: 'PUT', // Use 'PUT' for updating the profile
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
          email
        }),
      });

      if (response.ok) {
      	 	Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'User details Successfully Updated!'
			})
        console.log('Profile updated successfully!');
        refreshInput();
        fetchDetails();
      } else {
      	Swal.fire({
					title: 'Error',
					icon: 'error',
					text: 'Please try again'
				})
        console.error('Failed to update profile.');
        refreshInput();
        fetchDetails();
      }
    } catch (error) {
      console.error('Error occurred while updating profile:', error);
    }
  };

  const refreshInput = () => {
    setEmail('');
    
  }

  return (
    <Form onSubmit={handleFormSubmit}>
      <div className="form-group mb-3">
        <label htmlFor="email">Email</label>
        <input
          type="email"
          className="form-control"
          id="email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </div>

      
      <Button type="submit" className="btn btn-primary">Update Profile</Button>
    </Form>
  );
};

export default UpdateProfileForm;