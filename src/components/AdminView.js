import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct'
import { Link } from "react-router-dom";


export default function AdminView({productsData , fetchData}) {
	
	const [products, setProducts] = useState([]);



	
	useEffect(() => {
		const productsArr = productsData.map(product => {
			// console.log(product)
			return (
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td><EditProduct product={product._id} fetchData={fetchData}/></td>	
					<td><ArchiveProduct product={product._id} fetchData={fetchData} isActive={product.isActive}/></td>	
					
				</tr>
				)
		})

		setProducts(productsArr)

	}, [productsData])


	return(
		<>
			<h1 className="text-center my-4"> Admin Dashboard</h1>
			<div className="text-center my-4 ">
			<Button as={Link} to={`/addProduct`} variant="success" >Add Product</Button>
			<Button as={Link} to={`/orders`} variant="primary" >View All Orders</Button>
			</div>
			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th colSpan="2">Actions</th>
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>	
		</>

	)
}
