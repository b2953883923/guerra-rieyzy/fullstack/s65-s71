import { Form , Button, Container} from 'react-bootstrap';

import { useState , useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import { Navigate, Link } from 'react-router-dom'

import Swal from 'sweetalert2';



export default function Login(){
	
	const { user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState(false);

	console.log(email)
	console.log(password)

	function authenticateUser(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password

			})
		}).then(res => res.json())
			.then(data => {
			console.log(data)
			if(data.access){

				// Set the token of the authenticated user in the local storage.
				// Syntax:
					// localStorage.setItem("propertName", value)
				localStorage.setItem("token", data.access);

				// Sets the global user state to have the properties obtained from localStorage
				// setUser({
				//  access: localStorage.getItem("token")
				// })
				/*invoke the retrieveUserdetails*/
				retrieveUserDetails(data.access)

				// alert("Thank you for logging in.")
				Swal.fire({
					title: "Login Successfull",
					icon: "success",
					text: "Welcome to AD Sneaky PH!"
				})
			} else {
				// alert("Unsuccessful Login")
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})
			setEmail("");
			setPassword("");
	}

	const retrieveUserDetails = (token) => {

		// The token will be sent as part of the request's header information
		// We put "Bearer" in front of the token to follow the implementation standards for JWTs.
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(res => res.json())
		.then(data => {
			console.log(data);


			/* Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation accross the whole application*/
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})
	}	

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);
	
	return(
			(user.id !== null) ?
			<Navigate to="/products" />
			:
			<Container>
			 <div className="login-container">
			 <Form onSubmit={e => authenticateUser(e)}>
				<h1 className="my-5 text-center"> Login </h1>

			      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="name@example.com" required
			        value={email} onChange={e => {setEmail(e.target.value)}}
			        />
			      </Form.Group>

			    <Form.Group className="mb-3" controlId="Password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Enter password"  required
			        value={password} onChange={e => {setPassword(e.target.value)}}
			        />
			    </Form.Group>  

			    {
			    	isActive ?  <Button variant="primary" type="submit" id="submitBtn" className="login-button"> Submit </Button>
			    	:   <Button variant="danger" type="submit" id="submitBtn" className="login-button" disabled> Submit </Button>
			    }

			    <p className="my-3 text-center">
		          Don't have an account?{' '}
		          <Link to="/register">Register here</Link>
		        </p>

			</Form>
			</div>
			</Container>
	)
}