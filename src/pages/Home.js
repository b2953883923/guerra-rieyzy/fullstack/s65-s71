import Banner from '../components/Banner';
import FeaturedProducts from '../components/FeaturedProducts';
export default function Home() {

	const data = { title: "Make your move more comfortable", content: "100 % Legit", destination:"/products", label: "Buy Now!"};


	return(
		<>
			<Banner data={data} />
 			<FeaturedProducts/>
		</>

	)
};