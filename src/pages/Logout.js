import { Navigate} from 'react-router-dom';
import { useContext, useEffect } from 'react'
import UserContext from '../UserContext';

export default function Logout() {

	// the ".clear" method will delete the token stored in the local storage
	// localStorage.clear();

	const {unsetUser, setUser} = useContext(UserContext);

	// This invokes the unsetUser function from App.js to clear the data/toke from the local storage. This result to the value undefined

	unsetUser()

	useEffect(() => {	
		// set the user state back to its original value.
		setUser({
			id: null,
			isAdmin: null
		 })
	})

	return(

		<Navigate to="/login"/>
	) 
}