import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { FaCartShopping } from "react-icons/fa6";


export default function ProductView(){

	const { productId } = useParams();

	const { user } = useContext(UserContext);

	const [quantity, setQuantity] = useState(1);
	//an object with methods to redirect the user.
	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(productId)

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId] )


		const increaseQuantity = () => {
	    setQuantity(quantity + 1);
		  };

	  const decreaseQuantity = () => {
	    if (quantity > 1) {
	      setQuantity(quantity - 1);
	    }
	  };


	const Purchase = (productId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/orders/`, {
			method: "POST",
			headers: {
			  "Content-Type": "application/json",
			  Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
			  productId: productId,
			  quantity: quantity
			})
			})
			.then(res => res.json())
			.then(data => {

			console.log(data);
				if(data){
					Swal.fire({
						title: "Successfully purchase",
						icon: "success",
						text: "You have Successfully purchase for this item"
					})
					// allow us to navigate the user back to the course page programmatically instead of using component
					navigate("/products");
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
				}

			});
	}

	const AddToCart = (productId) => {

		fetch(`${ process.env.REACT_APP_API_URL }/carts/addToCart`, {
			method: "POST",
			headers: {
			  "Content-Type": "application/json",
			  Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
			  productId: productId,
			  quantity: quantity
			})
			})
			.then(res => res.json())
			.then(data => {

			console.log(data);
				if(data){
					Swal.fire({
						title: "Added to cart",
						icon: "success",
						text: "Added to cart"
					})
					// allow us to navigate the user back to the course page programmatically instead of using component
					navigate("/cart");
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
				}

			});
	}
	

	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text><span>&#8369;</span>{price}</Card.Text>

							<div className="d-flex justify-content-center align-items-center mb-3">
			                <Button
			                  variant="primary"
			                  className="me-2"
			                  onClick={decreaseQuantity}
			                >
			                  -
			                </Button>
			                <span>{quantity}</span>
			                <Button
			                  variant="primary"
			                  className="ms-2"
			                  onClick={increaseQuantity}
			                >
			                  +
			                </Button>

			              </div>
							
							
							{ user.id !== null 
							? 
							<>
							<div className="">
							<Button variant="primary" onClick={() => Purchase(productId)}>Purchase</Button>
							<Button variant="secondary" onClick={() => AddToCart(productId)}><FaCartShopping/></Button>
							</div>
							</>
							: <Button as={Link} to="/login" variant="danger">Login to Purchase</Button>
							}

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>




	)
}