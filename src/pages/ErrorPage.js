
import { Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";

  // data is inside the curly bracket because data is an object
export default function Banner() {

  return (

    <Row>
      <Col className="p-5 text-center">
        <h1>Page not found</h1>
        <p>Go back to</p>
        <Link className="btn btn-primary" to="/"> homepage </Link>
      </Col>
    </Row>


  )
};