import { Form , Button} from 'react-bootstrap';

import { useState , useEffect , useContext} from 'react';

import { Navigate , useNavigate} from 'react-router-dom'

// import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register(){

	// State Hooks to store the values of the register form input fields.

	// const { user , setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");

	


	// State to determine where submit is enabled or not
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();
	// check if the values are successfuly binded.
	// console.log(email);
	// console.log(password);
	

	function registerUser(e) {

		// Prevents the page redirection via form submission
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data) {
				Swal.fire({
					title: 'Email already exist',
					icon: 'error',
					text: 'Please try again!'
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data) {
						Swal.fire({
								title: 'Success!',
								icon: 'success',
								text: 'Successfully Registered!'
						})
						refreshInput()
						navigate('/login');
					} else {
						Swal.fire({
							title: 'Error',
							icon: 'error',
							text: 'Please try again!'
						})
					refreshInput();
					}

				})
			}


		})

	};

	const refreshInput = () => {
    setEmail('');
    setPassword('');
    setConfirmPassword('');
  }



	 useEffect(() => {

	 	if((email !== "" &&  password !== "" && confirmPassword !== "" ) && (password === confirmPassword)){

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email,password, confirmPassword]);

	 return (
		<div className="register-container">
		<Form onSubmit={e => registerUser(e)}>
		<h1 className="my-5 text-center"> Register </h1>

	      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="name@example.com" required
	        value={email} onChange={e => {setEmail(e.target.value)}}
	        />
	      </Form.Group>

	    <Form.Group className="mb-3" controlId="Password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Enter password"  required
	        value={password} onChange={e => {setPassword(e.target.value)}}
	        />
	    </Form.Group>

	    <Form.Group className="mb-3" controlId="Password2">
	        <Form.Label>Confirm Password:</Form.Label>
	        <Form.Control type="password" placeholder="Confirm Password" required
	        value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}
	        />
	    </Form.Group>

	   

	    {
	    	isActive ?  <Button variant="primary" type="submit" id="submitBtn" className="register-button"> Submit </Button>
	    	:   <Button variant="danger" type="submit" id="submitBtn" className="register-button" disabled> Submit </Button>
	    }

	    </Form>
	    </div>



	)
};

