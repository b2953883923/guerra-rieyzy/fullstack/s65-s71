import { useState, useEffect, useContext } from 'react';
import {  Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import ManageAdmin from '../components/ManageAdmin';

export default function ViewAllUsers(){

	 const { user } = useContext(UserContext);

	 const [users, setUsers] = useState([]);

	const fetchData = () => {

		fetch(`${process.env.REACT_APP_API_URL}/users/`)
        .then(res => res.json())
        .then(data => {
          // console.log(data)
          setUsers(data);
        })

	}


	useEffect(() => {

		fetchData();
	
	})

	return(
		
		<>
		{
		   	(user.isAdmin === true) ?
        	<ManageAdmin usersData={users} fetchData={fetchData}/>
         	:
         	<Navigate to="/users-all" />
        }
		</>	
	)	
}


