import { useState, useEffect, useContext } from 'react';
import { Container, Table } from 'react-bootstrap';
import {  Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function UserOrder() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);

  const navigate = useNavigate();

  useEffect(() => {
    // Fetch all orders for the currently logged-in user
    if (user.id) {
      fetch(`${process.env.REACT_APP_API_URL}/orders/user`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          setOrders(data.orders);
          navigate('/orders-user');
          const total = data.orders.reduce((acc, order) => acc + order.totalAmount, 0);
          setTotalAmount(total);
        })
        .catch(error => console.error(error));
    }
  }, [user]);

  return (
    (user.id === null) ?
        <Navigate to="/orders-user" />
        :
        <>
    <Container className="mt-5">
      <h2 className="text-center p-3">All Orders</h2>
      {orders?.length > 0 ? (
        <Table striped bordered responsive>
          <thead>
            <tr>
              <th>Order ID</th>
              <th>Product ID</th>
              <th>Quantity</th>
              <th>Purchased On</th>
              <th>Total Amount</th>
            </tr>
          </thead>
          <tbody>
            {orders.map(order => (
              <tr key={order._id}>
                <td>{order._id}</td>
                <td>{order.products[0].productId}</td>
                <td>{order.products[0].quantity}</td>
                <td>{order.purchasedOn}</td>
                <td>{order.totalAmount}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="4" className="text-end fw-bold">Total Amount of all Orders:</td>
              <td>{totalAmount}</td>
            </tr>
          </tfoot>
        </Table>
      ) : (
        <p>No orders found.</p>
      )}
    </Container>
    </>
  );
}
