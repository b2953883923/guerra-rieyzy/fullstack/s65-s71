import { useState, useEffect, useContext } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import {  Navigate } from 'react-router-dom';
import { FaMinus , FaCircleCheck} from "react-icons/fa6";
import UserContext from '../UserContext';

export default function Cart(){
	const { user } = useContext(UserContext);
	const [carts, setCarts] = useState([]);
	const [subTotal, setSubTotal] = useState(0);
	const [orderPlaced, setOrderPlaced] = useState(false);

	useEffect(() => {
    // Fetch all cart for the currently logged-in user
    if (user.id) {
      fetch(`${process.env.REACT_APP_API_URL}/carts/cart`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          setCarts(data.carts);
         
          const total = data.carts.reduce((acc, cart) => acc + cart.subTotal, 0);
          setSubTotal(total);
        })
        .catch(error => console.error(error));
    	}
  	}, [user]);

	const placeOrder = () => {
		fetch(`${ process.env.REACT_APP_API_URL}/carts/order`, {
		method: "POST",
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
	.then(response => response.json())
    .then(order => {
      console.log('Order placed:', order);
      setOrderPlaced(true);
      
    })
    .catch(error => console.error('Error placing order:', error));
	}

	return (
		(user.id === null || user.isAdmin === true) ?
        <Navigate to="/" />
        :
        <>
    <Container className="mt-5">
      <h2 className="text-center p-3">Cart</h2>
      {carts?.length > 0 ? (
        <Table striped bordered responsive>
          <thead>
            <tr>
              <th>Remove to Cart</th>
              <th>Order ID</th>
              <th>Product ID</th>
              <th>Quantity</th>
              <th>Purchased On</th>
              <th>Total Amount</th>
              <th>Order this item</th>
            </tr>
          </thead>
          <tbody>
            {carts.map(cart => (
              <tr key={cart._id}>
              	<td className="text-center"><Button variant="danger" onClick={placeOrder} disabled={orderPlaced}><FaMinus/></Button></td>
                <td>{cart._id}</td>
                <td>{cart.products[0].productId}</td>
                <td>{cart.products[0].quantity}</td>
                <td>{cart.createAt}</td>
                <td>{cart.products[0].subTotal}</td>
                <td className="text-center"><Button variant="success"><FaCircleCheck/></Button></td>
              </tr>
            ))}
          </tbody>
        </Table>
      ) : (
        <p>Your cart is empty.</p>
      )}
    </Container>
    </>
	)
}