import { Container, Form, Button} from 'react-bootstrap';
import { useState , useEffect , useContext} from 'react';
import Swal from 'sweetalert2';
import { Navigate} from 'react-router-dom'
import axios from 'axios';

import UserContext from '../UserContext';

export default function AddProduct(){

	const [isActive, setIsActive] = useState(false);

	const { user } = useContext(UserContext);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [image, setImage] = useState(null);

	// const handleFileChange = (e) => {
 //    setImage({ image, image: e.target.files[0] });
 //    };


	// function addProduct(e) {
 //    e.preventDefault();

 //    const formData = new FormData();
 //    formData.append('name', name);
 //    formData.append('description', description);
 //    formData.append('price', price);
 //    formData.append('image', image);

 //    axios
 //      .post(`${process.env.REACT_APP_API_URL}/products/new-product`, formData, {
 //        headers: {
 //          'Content-Type': 'multipart/form-data',
 //          Authorization: `Bearer ${localStorage.getItem('token')}`,
 //        },
 //      })
 //      .then((response) => {
 //        console.log(response.data);
 //        if (response.data) {
 //          setName('');
 //          setDescription('');
 //          setPrice('');
 //          setImage(null);

 //          Swal.fire({
 //            title: 'Product Added',
 //            icon: 'success',
 //          });
 //        } else {
 //          Swal.fire({
 //            title: 'Failed',
 //            icon: 'error',
 //            text: response.data.message,
 //          });
 //        }
 //      })
 //      .catch((error) => {
 //        console.error(error);
 //        Swal.fire({
 //          title: 'Failed',
 //          icon: 'error',
 //          text: 'Something went wrong while adding the product.',
 //        });
 //      });
 //  }

 	function addProduct(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/new-product`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data) {
				setName("");
				setDescription("");
				setPrice("");
				setImage(null);

				Swal.fire({
					title: "Product Added",
					icon: "success",
				})

			} else{
				Swal.fire({
					title: "Failed",
					icon: "error",
					text: data.message
				})
			}
		})
	}



	useEffect(() => {
		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price]);

	// function handleImageChange(e) {
 //    const file = e.target.files[0];
 //    setImage(file); // We store the selected file in the 'image' state for form submission.
 //  	}

	return (

		(user.isAdmin !== true)
		? 
		<Navigate to="/products" />
		: 
		<Container> 
			<Form onSubmit={e => addProduct(e)}>
				<h1 className="text-center"> Add Product </h1>

				<Form.Group className="mb-3" controlId="Last Name">
		        <Form.Label>Name</Form.Label>
		        <Form.Control type="text" placeholder="Enter Name" required 
		        value={name} onChange={e => {setName(e.target.value)}}
		        />
	   			</Form.Group>

	   			<Form.Group className="mb-3" controlId="Last Name">
		        <Form.Label>Description</Form.Label>
		        <Form.Control type="text" placeholder="Enter Description"
		         required 
		         value={description} onChange={e => {setDescription(e.target.value)}}
		         />
	   			</Form.Group>

	   			<Form.Group className="mb-3" controlId="Price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control type="number" placeholder="Enter Price" required 
		        value={price} onChange={e => {setPrice(e.target.value)}}
		        />
	   			</Form.Group>

	   			{/*<Form.Group controlId="imageInput">
	            <Form.Label>Select an image to upload:</Form.Label>
	            <Form.Control type="file" name="image" onChange={handleFileChange} />
	            </Form.Group>*/}

	   			{
			    	isActive ?  <Button variant="primary" type="submit" id="submitBtn"> Submit </Button>
			    	:   <Button variant="danger" type="submit" id="submitBtn" disabled> Submit </Button>
			    }
			</Form>
		</Container>
	)

}