import { BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom'
import {Container} from 'react-bootstrap';
import { useState, useEffect } from 'react';

import AppNavBar from './components/AppNavBar';
import Cart from './pages/Cart';
import ErrorPage from './pages/ErrorPage';
import Home from './pages/Home'
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout'
import AddProduct from './pages/AddProduct'
import Profile from './pages/Profile'
import Products from './pages/Products';
import ProductView from './pages/ProductView'
import Orders from './pages/Orders';
import UserOrder from './pages/UserOrder'
import ViewAllUsers from './pages/ViewAllUsers'

import './App.css';
import { UserProvider} from './UserContext';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

            fetch(`http://localhost:3001/users/details`, {
              headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
              }
            })
            .then(res => res.json())
            .then(data => {
              // console.log(data)
              // Set the user states values with the user details upon successful login.
              if (typeof data._id !== "undefined") {

                setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
                });

              // Else set the user states to the initial values
              } else {

                setUser({
                  id: null,
                  isAdmin: null
          });

        }

      })

    }, []);

  useEffect(() => {
    // console.log(user);
    // console.log(localStorage);
  }, [user]) 

  return (
   
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
         <AppNavBar/>
         <Container>
           <Routes>
             <Route path="/" element={<Home/>} />
             <Route path="/addProduct" element={<AddProduct/>} />
             <Route path="/cart" element={<Cart/>} />
             <Route path="/profile" element={<Profile/>} />
             <Route path="/products" element={<Products/>} />
             <Route path="/products/:productId" element={<ProductView/>} />
             <Route path="/register" element={<Register/>} />
             <Route path="/login" element={<Login/>} />
             <Route path="/logout" element={<Logout/>} />
             <Route path="/orders" element={ <Orders /> } />
             <Route path="/orders-user" element={ <UserOrder /> } />
             <Route path="/users-all" element={ <ViewAllUsers/> } />
             <Route path="/*" element={ <ErrorPage /> } />
           </Routes>
           </Container>
      </Router>
    </UserProvider>
   
  );
}

export default App;
